#!/usr/bin/bash

# Check command line argument
if [ -z "$1" ]; then
	echo "Data directory not specified!"
	exit 2
fi
if ! [ -e "$1" ]; then
	echo "Directory $1 does not exist!"
	exit 1
fi
if ! [ -d "$1" ]; then
	echo "Path $1 is not a directory!"
	exit 2
fi

# Check for config file
if ! [ -e "$1/config.list" ]; then
	echo "Config file does not exist in $1!"
	exit 1
fi
if ! [ -r "$1/config.list" ]; then
	echo "Can't read $1/config.list!"
	exit 1
fi

export _DATA="$1"

function _update {
	case "$1" in
		# Git
		git )
			# Check if directory exists yet, and if not, first time clone
			if ! [ -d "$2" ]; then
				echo "$2 does not exist, cloning from $3..."
				# consider looking into --[no-]reject-shallow
				# consider using --bare
				# consider using --no-single-branch, but I can't tell if this is beneficial
				# consider using --recurse-submodules=. to get all submodules
				git clone "$3" "$2"
			# Otherwise, cd into directory, and check for and pull down latest changes
			else
				if ! cd "$2"; then
					return 1
				fi
				echo "fetching changes to $3"
				# consider --force
				# consider --prune, but some people might like the extra data...
				# consider --recurse-submodules=yes
				#git fetch --all --tags
				git pull --all --tags
				cd ..
			fi
			;;



		github )
			if ! command -v jq &>/dev/null; then
				echo "jq not installed, this is required to parse GitHub's API!"
				return 1
			fi
			if ! [ -d "$2" ]; then
				mkdir "$2"
			fi
			if ! cd "$2"; then
				return 1
			fi
			for repo in $(curl -L "https://api.github.com/users/$3/repos" | jq --raw-output '.[] | .name'); do
				_update git "$repo" "https://github.com/$3/$repo"
			done
			cd ..
			;;



		gitlab )
			if ! command -v jq &>/dev/null; then
				echo "jq not installed, this is required to parse GitLab's API!"
				return 1
			fi
			if ! [ -d "$2" ]; then
				mkdir "$2"
			fi
			if ! cd "$2"; then
				return 1
			fi
			for repo in $(curl -L "https://gitlab.com/api/v4/users/$3/projects" | jq --raw-output '.[] | .name'); do
				_update git "$repo" "https://gitlab.com/$3/$repo"
			done
			cd ..
			;;



		svn )
			echo "SVN Not Implemented Yet!"
			return 1
			;;



		* ) echo "Unknown vcs type '$1'"; return 2 ;;
	esac
}
export -f _update

# For each non-empty, non comment line in config.list:
cd "$_DATA"
awk -F/ '!/^($|#)/{for(i=4;i<=NF;i++){$3=$3 "/" $i}; system("_update \"" $1 "\" \"" $2 "\" \"" $3 "\"") }' "config.list"
