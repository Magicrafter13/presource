# DISCLAIMER
This software is very new. It seems to be working right now, but I make no
remote guarantee. But I encourage you to test the software and report any bugs.

# PreSource
A combination of the words "source" and "preserver". This software is designed
for software preservation, more specifically source code. Originally imagined as
a way of keeping git repositories up to date, I realized it should be able to
support all kinds of source control systems.

# What It Supports
Currently it only supports `git`. If I find something I want to preserve that's
an SVN repo, or some other system, I will add it to the project. Or if I just
decided to do it because I'm bored. Or if someone submits a merge request.

Also it assumes you're on a system that uses cron, as this collection of scripts
is executed as a cron job.

# How to Use It
Simply run `./install.sh` and it will guide you through the process.

# History
This project was created shortly following Nintendo's lawsuit against "Tropic
Haze". I won't go too in-depth here, but suffice to say I do NOT support what
happened in any way. And, as I find myself being worried about other projects
suffering similar fates, I wanted to make sure I could have rolling backups of
software I care about.
