#!/usr/bin/bash

file=./

# To be set by CLI arguments.
YES=
if [ "$1" == "-y" ]; then
	YES=yes
fi

# Default values
runas=root
storein=/var/lib/presource
link=no

# Determine if root, so we can install to /etc/cron.d, otherwise we have to actually edit the user's crontab.
if [ "$EUID" -eq 0 ]; then
	# Ask user if they want to run as root
	_yn='y'
	if [ -z "$YES" ]; then
		echo -e "By default this script will run as root (and therefore, all files will be owned\nby root), is this okay?"
		while true; do
			read -p "y/N > " _yn
			case "$_yn" in
				[Yy]* ) break;;
				[Nn]* ) _yn=n; break;;
			esac
		done
	fi
	if [ "$_yn" == n ]; then
		echo "Enter the user to run the script as:"
		while true; do
			read -p "> " runas
			if [ -n "$runas" ] && [ "$runas" != root ] && grep -E "^$runas(:.*){6}" /etc/passwd &>/dev/null; then
				break
			fi
		done
	fi
	#echo "RUNNING AS $runas"

	# Ask user if they want to store data in /var/lib
	_yn='y'
	if [ -z "$YES" ]; then
		echo "By default, repositories are downloaded to $storein, is this okay?"
		while true; do
			read -p "y/N > " _yn
			case "$_yn" in
				[Yy]* ) break;;
				[Nn]* ) _yn=n; break;;
			esac
		done
	fi
	if [ "$_yn" == n ]; then
		echo "Enter path to store repositories:"
		while true; do
			read -p "> " storein
			if [ -n "$storein" ] && ([ -d "$storein" ] || (mkdir -p "$storein" && echo "Created directory: $storein")); then # check if dir exists, if not try to make it
				break
			fi
		done
	fi
	#echo "STORING IN $storein"

	# Ask user if they want updates to automatically apply
	_yn='y'
	if [ -z "$YES" ]; then
		echo -e "By default, you will have to run this script every time the project is updated.\nIf you wish, we can hard link the presource script, so all you have to do is run\n'git pull'. This will present a security risk however, as anyone who can edit\nthe file here effectively gains ROOT ACCESS TO YOUR MACHINE. Would you like to\nkeep the default SAFE setting?"
		while true; do
			read -p "y/N > " _yn
			case "$_yn" in
				[Yy]* ) break;;
				[Nn]* ) _yn=n; break;;
			esac
		done
	fi
	if [ "$_yn" == n ]; then
		echo "Are you absolutely sure? You are fully responsible for what happens."
		while true; do
			read -p "y/N > " _yn
			case "$_yn" in
				[Yy]* ) link=yes; break;;
				[Nn]* ) break;;
			esac
		done
	fi

	# push job to /etc/cron.d
	echo "Creating cron job."
	echo "0 * * * * $runas presource $storein" | cat cron-template - > /etc/cron.d/presource-hourly

	# push script to /usr/local/sbin
	if [ $link == yes ]; then
		echo "Hard linking presource to /usr/local/sbin/"
		ln presource /usr/local/sbin/
	else
		echo "Copying presource to /usr/local/sbin/"
		cp presource /usr/local/sbin/
	fi

	# set mode
	echo "Setting mode of /usr/local/sbin/presource to 744."
	chmod 744 /usr/local/sbin/presource
else
	# Default values
	runas=$USER
	storein="${XDG_DATA_HOME:-$HOME/.local/share}"/presource

	# Ask user if they want to store data in $XDG_DATA_HOME
	_yn='y'
	if [ -z "$YES" ]; then
		echo "By default, repositories are downloaded to $storein, is this okay?"
		while true; do
			read -p "y/N > " _yn
			case "$_yn" in
				[Yy]* ) break;;
				[Nn]* ) _yn=n; break;;
			esac
		done
	fi
	if [ "$_yn" == n ]; then
		echo "Enter path to store repositories:"
		while true; do
			read -p "> " storein
			if [ -n "$storein" ] && ([ -d "$storein" ] || (mkdir -p "$storein" && echo "Created directory: $storein")); then # check if dir exists, if not try to make it
				break
			fi
		done
	fi
	#echo "STORING IN $storein"

	# push job to /etc/cron.d
	echo "Creating cron job."
	if ! crontab -l > .tmp; then
		exit 1
	fi
	echo "0 * * * * /usr/bin/bash $PWD/presource $storein" >> .tmp
	crontab .tmp
	rm .tmp
fi

if ! [ -e "$storein" ]; then
	echo "Creating $storein if it doesn't exist."
	mkdir -p $storein
fi

if ! [ -e "$storein/config.list" ]; then
	echo "Copying default configuration to $storein/config.list."
	cp config.list "$storein/"
fi

echo "Done!"
